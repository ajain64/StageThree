init:
	pip install --user --ignore-installed -r requirements.txt
	pip3 install pyqt5

test:
	nosetests --nocapture tests
