from bs4 import BeautifulSoup
import os
import glob
import codecs
import csv
import sys

XML_FILE_EXTENSION = '*.html'
CSV_FILE_EXTENSION = '.csv'
WEBDATA_RELATIVE_PREFIX = '../data/srcTwo/'
CSV_OUTPUT_FILE = WEBDATA_RELATIVE_PREFIX + 'bnn_final' + CSV_FILE_EXTENSION
REPLACE_COMMA = '#'
COMMA = ","
HYPHEN = "-"

Title = "Title"
Author = "Author"
Type = "Type"
Pages = "Pages"
Publisher = "Publisher"
Language = "Language"
ISBN_10 = "ISBN-10"
ISBN_13 = "ISBN-13"
Age_Range = "Age Range"
Grade_Level = "Grade Level"
Product_Dimension = "Product Dimensions"
Shipping_Weight = "Shipping Weight"
Price = "Price"
Publication_Date = "Publication Date"
ID = "Id"

# BNN
Edition_Description = "Edition Description"
Sales_Rank = "Sales Rank"
Average_Review = "Average Review"
Lexile = "Lexile"
Series = "Series"
Sold_By = "Sold By"
Format = "Format"

# OUTPUT_FORMAT = [Title, Author, Pages, Publisher, ISBN_13, Age_Range, Product_Dimension, Price]

OUTPUT_FORMAT = [ID, Title, Author, Publisher, Publication_Date, Price, Product_Dimension, Pages, Age_Range, ISBN_13,
                 ISBN_10, Language, Type, Grade_Level, Shipping_Weight, Edition_Description, Sales_Rank, Average_Review,
                 Lexile, Series, Sold_By, Format]


def generate_xml_files():
    reload(sys)
    sys.setdefaultencoding('utf8')
    path = get_file_path(WEBDATA_RELATIVE_PREFIX) + XML_FILE_EXTENSION
    files = glob.glob(path)
    mapList = []
    id = 1
    for file_name in files:
        print file_name
        d = {}
        f = codecs.open(file_name, 'r', encoding="utf-8")
        xml_content = f.read()
        soup = BeautifulSoup(xml_content, 'html.parser')
        fill_default_values(d)
        get_attributes(soup, d)  # get data from table
        if check_validity(d) < 0:
            continue
        d[ID] = "BNN#" + str(id)
        id = id + 1
        mapList.append(d)
        # print(d.keys())
        # count += 1
        # if count == 10:
        #    break
    # print mapList
    keys = []
    bookList = []
    for m in mapList:
        for k in m.keys():
            if k not in keys:
                keys.append(k)

    for m in mapList:
        book = []
        for i in range(len(OUTPUT_FORMAT)):
            book.append("")
        for k in keys:
            if k in m.keys() and k in OUTPUT_FORMAT:
                # book.append(m[k].replace(',', REPLACE_COMMA))
                # book.append(m[k])
                book[OUTPUT_FORMAT.index(k)] = m[k].encode('utf-8')
            else:
                print "SHOULD NOT COME HERE"
                book.append('')
        add_quotes_to_comma_attr(book)
        bookList.append(book)

    # print(bookList)
    # print(keys)
    write_to_file(bookList, keys)


def check_validity(book_details):
    if book_details is None:
        return -1
    if book_details[Title] == "":
        return -1
    return 0


def add_quotes_to_comma_attr(book_details):
    if book_details is None:
        return

    book_details[OUTPUT_FORMAT.index(Title)] = add_quotes(book_details[OUTPUT_FORMAT.index(Title)])
    book_details[OUTPUT_FORMAT.index(Author)] = add_quotes(book_details[OUTPUT_FORMAT.index(Author)])
    book_details[OUTPUT_FORMAT.index(Publisher)] = add_quotes(book_details[OUTPUT_FORMAT.index(Publisher)])
    book_details[OUTPUT_FORMAT.index(Series)] = add_quotes(book_details[OUTPUT_FORMAT.index(Series)])
    book_details[OUTPUT_FORMAT.index(Language)] = add_quotes(book_details[OUTPUT_FORMAT.index(Language)])
    book_details[OUTPUT_FORMAT.index(Sales_Rank)] = add_quotes(book_details[OUTPUT_FORMAT.index(Sales_Rank)])
    book_details[OUTPUT_FORMAT.index(Edition_Description)] = add_quotes(
        book_details[OUTPUT_FORMAT.index(Edition_Description)])
    book_details[OUTPUT_FORMAT.index(Sold_By)] = add_quotes(book_details[OUTPUT_FORMAT.index(Sold_By)])

    if HYPHEN in book_details[OUTPUT_FORMAT.index(ISBN_13)]:
        book_details[OUTPUT_FORMAT.index(ISBN_13)] = book_details[OUTPUT_FORMAT.index(ISBN_13)].replace(HYPHEN, "")
    # book_details[OUTPUT_FORMAT.index(ISBN_13)] = add_quotes(book_details[OUTPUT_FORMAT.index(ISBN_13)])

    if HYPHEN in book_details[OUTPUT_FORMAT.index(ISBN_10)]:
        book_details[OUTPUT_FORMAT.index(ISBN_10)] = book_details[OUTPUT_FORMAT.index(ISBN_10)].replace(HYPHEN, "")
    # book_details[OUTPUT_FORMAT.index(ISBN_10)] = add_quotes(book_details[OUTPUT_FORMAT.index(ISBN_10)])

    pages = book_details[OUTPUT_FORMAT.index(Pages)]
    if "pages" in pages:
        pages = pages.replace("pages", "")
    if COMMA in pages:
        pages = pages.replace(COMMA, "")
    book_details[OUTPUT_FORMAT.index(Pages)] = pages


def add_quotes(value):
    if value is None or value == "" or len(value) == 0:
        return value
    return "\"" + value + "\""


def fill_default_values(d):
    for i in OUTPUT_FORMAT:
        d[i] = ""


def print_map(map):
    for k, v in map.items():
        print (k + " ---> " + v)


def write_to_file(book_list, keys):
    create_file_path = get_file_path(CSV_OUTPUT_FILE)

    # columns = [keys]
    columns = OUTPUT_FORMAT
    with open(create_file_path, 'wb') as to_write_file:
        writer = csv.writer(to_write_file)
        writer.writerow(columns)
        for book in book_list:
            writer.writerow(book)


def findnth(haystack, needle, n):
    parts = haystack.split(needle, n + 1)
    if len(parts) < n + 1:
        return -1
    return len(haystack) - len(parts[-1]) - len(needle)


def get_attributes(soup, d):
    details = soup.find('div', id="ProductDetailsTab")
    # print(details)
    title = soup.find('div', id='pdp-header-info')
    author = soup.find('span', id='key-contributors')
    price = soup.find('span', id='pdp-cur-price')
    reviews = soup.find('span', attr={'class': 'gig-average-review'})
    rows = details.findAll('td')
    # for r in rows:
    #    print(r.get_text().rstrip().lstrip())

    rowHeads = details.findAll('th')
    for i in range(len(rowHeads)):
        if rowHeads[i].get_text().rstrip().lstrip()[:-1] == 'Lexile':
            d[rowHeads[i].get_text().rstrip().lstrip()[:-1]] = rows[i].get_text().rstrip().lstrip().split()[0]
        elif "dimensions" in rowHeads[i].get_text().rstrip().lstrip()[:-1]:
            d[Product_Dimension] = rows[i].get_text().rstrip().lstrip()
        elif "date" in rowHeads[i].get_text().rstrip().lstrip()[:-1]:
            d[Publication_Date] = rows[i].get_text().rstrip().lstrip()
        elif "rank" in rowHeads[i].get_text().rstrip().lstrip()[:-1]:
            d[Sales_Rank] = rows[i].get_text().rstrip().lstrip()
        elif "description" in rowHeads[i].get_text().rstrip().lstrip()[:-1]:
            d[Edition_Description] = rows[i].get_text().rstrip().lstrip()
        elif "Sold" in rowHeads[i].get_text().rstrip().lstrip()[:-1]:
            d[Sold_By] = rows[i].get_text().rstrip().lstrip()
        else:
            d[rowHeads[i].get_text().rstrip().lstrip()[:-1]] = rows[i].get_text().rstrip().lstrip()

    author = author.get_text().rstrip().lstrip().replace('by', '').replace('\n', '')
    i = findnth(author, author.split()[0], 2)
    # print(author.split(author.split()[0]))
    # print(author[:i])
    # print(title.get_text().rstrip().lstrip().split('by')[0].replace('\n',''))


    d['Title'] = title.get_text().rstrip().lstrip().split('by')[0].replace('\n', '')
    d['Author'] = author[1:i]
    if price == None:
        price = soup.find('span', id='pdp-cur-price-BuyNew')
        if price == None:
            d['Price'] = '-1'
        else:
            d['Price'] = price.get_text()
    else:
        d['Price'] = price.get_text()
    if reviews == None:
        d['Average Review'] = '-1'
    else:
        d['Average Review'] = reviews.get_text()

    # print(d)
    return d


def get_file_path(file_name):
    current_file_path = os.path.dirname(__file__)
    return os.path.join(current_file_path, file_name)


generate_xml_files()
