from bs4 import BeautifulSoup
import os
import glob
import codecs
import csv
import sys

INPUT_FILE_EXTENSION = '*.html'
CSV_FILE_EXTENSION = '.csv'
WEBDATA_RELATIVE_PREFIX = '../data/srcOne/'
CSV_OUTPUT_FILE = "amazon_final" + CSV_FILE_EXTENSION
COMMA_REPLACEMENT = "#"
COMMA = ","
HYPHEN = "-"

Title = "Title"
Author = "Author"
Type = "Type"
Pages = "Pages"
Publisher = "Publisher"
Language = "Language"
ISBN_10 = "ISBN-10"
ISBN_13 = "ISBN-13"
Age_Range = "Age Range"
Grade_Level = "Grade Level"
Product_Dimension = "Product Dimensions"
Shipping_Weight = "Shipping Weight"
Price = "Price"
Publication_Date = "Publication Date"
ID = "Id"

# BNN
Edition_Description = "Edition Description"
Sales_Rank = "Sales Rank"
Average_Review = "Average Review"
Lexile = "Lexile"
Series = "Series"
Sold_By = "Sold By"
Format = "Format"

OUTPUT_FORMAT = [ID, Title, Author, Publisher, Publication_Date, Price, Product_Dimension, Pages, Age_Range, ISBN_13,
                 ISBN_10, Language, Type, Grade_Level, Shipping_Weight, Edition_Description, Sales_Rank, Average_Review,
                 Lexile, Series, Sold_By, Format]
TYPE_OPTIONS = ["Paperback", "Hardcover"]


def generate_xml_files():
    reload(sys)
    sys.setdefaultencoding('utf8')
    book_list = []
    attribute_set = set()

    path = get_file_path(WEBDATA_RELATIVE_PREFIX) + INPUT_FILE_EXTENSION
    files = glob.glob(path)
    id = 1
    for file_name in files:
        # features = OrderedDict()
        print file_name
        book_details = []
        fill_default_value(book_details)
        f = codecs.open(file_name, 'r', encoding="utf-8")
        xml_content = f.read()
        soup = BeautifulSoup(xml_content, 'html.parser')
        get_name(soup, book_details)  # add name and author in features
        get_price(soup, book_details)
        get_author(soup, book_details)
        list = get_other_attributes(soup, file_name)  # get data from table

        # add_attributes_to_set(list, attribute_set)

        fill_book_details(list, book_details)
        add_quotes_to_comma_attr(book_details)
        if check_validity(book_details) < 0:
            continue
        book_details[OUTPUT_FORMAT.index(ID)] = "Amazon#" + str(id)
        id = id + 1
        book_list.append(book_details)

    write_to_file(book_list)
    # print_set(attribute_set)


def check_validity(book_details):
    if book_details is None:
        return -1
    if book_details[OUTPUT_FORMAT.index(Title)] == "":
        return -1
    return 0


def add_quotes_to_comma_attr(book_details):
    if book_details is None:
        return

    book_details[OUTPUT_FORMAT.index(Title)] = add_quotes(book_details[OUTPUT_FORMAT.index(Title)])
    book_details[OUTPUT_FORMAT.index(Author)] = add_quotes(book_details[OUTPUT_FORMAT.index(Author)])
    book_details[OUTPUT_FORMAT.index(Publisher)] = add_quotes(book_details[OUTPUT_FORMAT.index(Publisher)])
    book_details[OUTPUT_FORMAT.index(Series)] = add_quotes(book_details[OUTPUT_FORMAT.index(Series)])
    book_details[OUTPUT_FORMAT.index(Language)] = add_quotes(book_details[OUTPUT_FORMAT.index(Language)])
    book_details[OUTPUT_FORMAT.index(Sales_Rank)] = add_quotes(book_details[OUTPUT_FORMAT.index(Sales_Rank)])
    book_details[OUTPUT_FORMAT.index(Edition_Description)] = add_quotes(book_details[OUTPUT_FORMAT.index(Edition_Description)])
    book_details[OUTPUT_FORMAT.index(Sold_By)] = add_quotes(book_details[OUTPUT_FORMAT.index(Sold_By)])

    if HYPHEN in book_details[OUTPUT_FORMAT.index(ISBN_13)]:
        book_details[OUTPUT_FORMAT.index(ISBN_13)] = book_details[OUTPUT_FORMAT.index(ISBN_13)].replace(HYPHEN,"")
    #book_details[OUTPUT_FORMAT.index(ISBN_13)] = add_quotes(book_details[OUTPUT_FORMAT.index(ISBN_13)])

    if HYPHEN in book_details[OUTPUT_FORMAT.index(ISBN_10)]:
        book_details[OUTPUT_FORMAT.index(ISBN_10)] = book_details[OUTPUT_FORMAT.index(ISBN_10)].replace(HYPHEN,"")
    #book_details[OUTPUT_FORMAT.index(ISBN_10)] = add_quotes(book_details[OUTPUT_FORMAT.index(ISBN_10)])

    # Replacing comma in date
    if COMMA in book_details[OUTPUT_FORMAT.index(Publication_Date)]:
        book_details[OUTPUT_FORMAT.index(Publication_Date)] = book_details[OUTPUT_FORMAT.index(Publication_Date)].replace(COMMA,"")

    pages = book_details[OUTPUT_FORMAT.index(Pages)]
    if "pages" in pages:
        pages = pages.replace("pages","")
    if COMMA in pages:
        pages = pages.replace(COMMA,"")
    book_details[OUTPUT_FORMAT.index(Pages)] = pages

def add_quotes(value):
    if value is None or value == "" or len(value) == 0:
        return value
    return "\"" + value + "\""


def fill_default_value(book_details):
    for i in range(len(OUTPUT_FORMAT)):
        book_details.append("")


def add_attributes_to_set(list, set):
    if list is None or len(list) == 0:
        return

    for feature in list:
        tuple = feature.get_text().encode('utf8')
        set.add(tuple)
        # key = tuple[0].strip()
        # value = tuple[1].strip()
        # set.add(key)
        # set.add(value)


def print_map(map):
    for k, v in map.items():
        print k + " ---> " + v


def print_set(attribute_set):
    for element in attribute_set:
        print element
        print '\n'


def fill_book_details(list, book_details):
    if list is None or len(list) == 0:
        return book_details
    for feature in list:
        # print feature
        tuple = feature.get_text().encode('utf8').split(":")
        if tuple is None or len(tuple) < 2:
            continue
        key = tuple[0].strip()
        value = tuple[1].strip()

        if key in TYPE_OPTIONS:
            book_details[OUTPUT_FORMAT.index(Type)] = key
            book_details[OUTPUT_FORMAT.index(Pages)] = value
        else:
            if (key in OUTPUT_FORMAT):
                if key == Shipping_Weight:
                    value = value.split("(")[0]
                # book_details[OUTPUT_FORMAT.index(key)] = value.replace(",",COMMA_REPLACEMENT)
                if key == Publisher:
                    if "(" in value:
                        index = value.index("(")
                        book_details[OUTPUT_FORMAT.index(Publication_Date)] = value[index + 1:-1]
                        value = value[0:index]

                book_details[OUTPUT_FORMAT.index(key)] = value
            else:
                print "NEW FEATURE " + str(key) + " DETECTED"
    return book_details


def write_to_file(book_list):
    create_file_path = get_file_path(WEBDATA_RELATIVE_PREFIX + CSV_OUTPUT_FILE)
    columns = OUTPUT_FORMAT
    with open(create_file_path, 'wb') as to_write_file:
        writer = csv.writer(to_write_file)
        writer.writerow(columns)
        for book in book_list:
            # print book
            writer.writerow(book)


def get_price(soup, book_details):
    # find prices
    # book_details[OUTPUT_FORMAT.index(Price)] = 0
    buy_panel = soup.find('div', id="buyNewSection")
    if (buy_panel is None or len(buy_panel) == 0):
        return book_details;

    inline_block = buy_panel.find("div", class_="inlineBlock-display")
    rows = inline_block.findAll("span")
    for row in rows:
        if "$" in row.get_text().encode('utf8'):
            book_details[OUTPUT_FORMAT.index(Price)] = row.get_text().encode('utf8')


def get_author(soup, book_details):
    # find prices
    buy_panel = soup.findAll('span', class_="author notFaded")
    if (buy_panel is None or len(buy_panel) == 0):
        print "No Author in book title " + book_details[OUTPUT_FORMAT.index(Title)]
        return book_details

    for panel in buy_panel:
        inline_block = panel.findAll("a", class_="a-link-normal contributorNameID")
        if (inline_block is not None and len(inline_block) > 0):
            for row in inline_block:
                # book_details[OUTPUT_FORMAT.index(Author)] = row.get_text().replace(",", COMMA_REPLACEMENT).encode('utf8')+COMMA_REPLACEMENT
                book_details[OUTPUT_FORMAT.index(Author)] = row.get_text().encode(
                    'utf8') + COMMA
        # using field-author field
        else:
            field_author_list = panel.findAll("a", href=True)
            for tuple in field_author_list:
                href = tuple['href']
                if "field-author" in href:
                    # ook_details[OUTPUT_FORMAT.index(Author)] += tuple.get_text().replace(",", COMMA_REPLACEMENT).encode('utf8') + COMMA_REPLACEMENT
                    book_details[OUTPUT_FORMAT.index(Author)] += tuple.get_text().encode('utf8') + COMMA

    book_details[OUTPUT_FORMAT.index(Author)] = book_details[OUTPUT_FORMAT.index(Author)][:-1]


def get_other_attributes(soup, file_name):
    rv = []
    table = soup.find('table', id="productDetailsTable")
    if table is None or len(table) == 0:
        return rv

    rows = table.findAll('tr')
    for row in rows:
        li_list = row.findAll('li')
        for li in li_list[:7]:
            rv.append(li);
    if len(rv) == 0:
        print 'No content found for: ' + file_name
    return rv


def get_name(soup, book_details):
    title = soup.find('span', id='productTitle')
    # print(title.get_text())
    if (title is not None):
        # book_details.insert(OUTPUT_FORMAT.index(Title), (title.get_text().replace(",",COMMA_REPLACEMENT)))
        book_details.insert(OUTPUT_FORMAT.index(Title), (title.get_text()))
    '''
    authors = soup.find_all('span', attrs={'class': "a-size-medium"})

    # print authors
    for a in authors:
        # print(a.parent.parent.parent.parent.parent.parent.parent.name)
        if a.parent.parent.parent.parent.parent.parent.parent.parent.name == 'span':
            # print(' '.join(a.get_text().split()[:-1]))
            book_details[OUTPUT_FORMAT.index(Author)] = ' '.join(a.get_text().split()[:-1])
            break

    authors = soup.find_all('span', attrs={'class': 'author notFaded'})

    # for multiple authors
    for a in authors:
        if a.find('a', attrs={'class': 'a-link-normal'}) and a.parent.name == 'span':
            # print(a.get_text())
            book_details[OUTPUT_FORMAT.index(Author)] += ';' + a.get_text()
    '''
    return book_details


def get_file_path(file_name):
    current_file_path = os.path.dirname(__file__)
    return os.path.join(current_file_path, file_name)


generate_xml_files()